<!DOCTYPE html>
<?php
    // include('db_connect.php');
    $connect = new mysqli('localhost','root','', 'batch2021');
    if($connect->connect_error){
        die('<h1>Could not connect to database!</h1>');
    }
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Validation</title>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <script src="../jquery.js"></script>
</head>
<body style="background:url('../images/nature.jpg');background-size:cover;" class="pt-5">
    <div class="container w-50 p-4 mt-5" style="background:rgba(0,0,0,0.5);color:white;">
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-light text-center">Create Account</h3>
            </div>
            <div class="col-md-12">
                <?php
                
                if(isset($_POST['register'])){
                    $name = $_POST['full_name'];
                    $email = $_POST['email'];
                    $pass = md5($_POST['password']);
                    $cpass = $_POST['cpassword'];
                    $gen = $_POST['gender'];

                    $checkQuery = "SELECT * FROM register WHERE email = '$email'";
                    $result = $connect->query($checkQuery);
                    $num = mysqli_num_rows($result);
                    
                    if($num==0){
                        $insertQuery = "INSERT INTO register(name, email, password, gender) VALUES ('$name','$email','$pass','$gen')";
                        if($connect->query($insertQuery)){
                            echo '<p class="alert alert-success">Registered Successfully!</p>';
                            header('Location:../21-May-2021/all_users.php');
                        }else{
                            echo '<p class="alert alert-danger">Error '.$connect->error.'</p>';
                        }
                    }else{
                        echo '<p class="alert alert-danger">A user with '.$email.' email already exists!</p>';
                    }
                }
                    
                ?>
                <form action="" method="post">
                    <div class="form-group">
                        <label for="">Enter Name</label>
                        <input type="text" name="full_name" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">Enter Email</label>
                        <input type="email" name="email" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">Enter Password</label>
                        <input type="password" name="password" class="form-control" id="pass" onkeyup="check_password()">
                    </div>
                    <div class="form-group">
                        <label for="">Confirm Password</label>
                        <input type="password" name="cpassword" class="form-control" id="cpass" onkeyup="check_password()">
                    </div>
                    <div class="form-group">
                        <label for="male"><input type="radio" value="male" name="gender" checked> Male</label>
                        <label for="female"><input type="radio" value="female" name="gender"> Female</label>
                    </div>
                    <div class="form-group">
                        <input type="submit" id="register" value="Create Account" class="btn btn-success" name="register">
                    </div>
                    <div class="form-group text-light">
                        <p>Alraedy Have an Account?
                        <a href="../28-May-2021/login.php">Click to Login</a>
                        </p>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
    function check_password(){
        var pass = document.getElementById('pass').value;
        var cpass = document.getElementById('cpass').value;

        if(pass!=cpass){
            $("#pass").addClass('alert alert-danger');
            $("#cpass").addClass('alert alert-danger');
            $("#register").attr('disabled',true);
        }else{
            $("#pass").removeClass('alert alert-danger');
            $("#cpass").removeClass('alert alert-danger');
            $("#register").removeAttr('disabled');
        }
    }
    </script>
</body>
</html>