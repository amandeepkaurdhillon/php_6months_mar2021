<?php 

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class UserController extends Controller{
    public function index(){
        // // Insert data 
        // DB::insert('insert into users(name, email, password) VALUES("Amandeep Kaur", "amandeep@gmail.com","123")');
        // return "Inserted!";

        // Update Query 
        // DB::update("UPDATE users set email='amandeep.kaur@gmail.com' WHERE id='3'");
        DB::update("UPDATE users set email=? WHERE id='1'",['er.amandeep@gmail.com']);

        // Delete Query 
        DB::delete("DELETE FROM users WHERE id='3'");

        // // Retrieve data 
        // $users =  DB::select("select * from users");

        // Insert data to User model
        $user = new User();
        $user->name = "James Parker";
        $user->email = "james.parker@gmail.com";
        $user->password = "1";
        $user->save();
        return "Saved Successfully!";

        // Retrieve all users
        // return User::find(5);
        // User::find(7)->delete();
        // User::where('id',5)->delete();
        
        User::where('id',8)->update([
            'name'=>'Mr. Parker',
            'email'=>'parker@gmail.com',
        ]);

        return User::all();
        
        // return view('first');
    }
    public function abcd(){
        return "I am another function in user controller";
    }
}