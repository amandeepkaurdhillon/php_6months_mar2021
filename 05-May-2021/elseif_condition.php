<?php 
date_default_timezone_set('Asia/Kolkata');
$d = date('l');
// $d = 'Sunday';

if($d=='Monday'){
    echo 'Today I will go to gym<br/>';
}
elseif($d=='Tuesday'){
    echo 'Today I will go to temple <br/>';
}
elseif($d=='Wednesday'){
    //nested if
    echo "Today is ".$d.'<br/>';

    $time = date('a');
    if($time=='pm'){
        echo '<h1 style="color:green;">I have an important meeting today!</h1>';
    } else{
        echo '<h1 style="color:red;">I have to return home!</h1>';
    }
   
}
else{
    echo 'I have not decided yet!!!';
}

?>