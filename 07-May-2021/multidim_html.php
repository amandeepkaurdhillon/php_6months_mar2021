<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Multidimentional Array</title>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
</head>
<body>

    <?php 
    $employees = array(
        array('name'=>'Peter Parker','age'=>22,'salary'=>30000,'pic'=>'1.jpg'),
        array('name'=>'Schinchan Nohara','age'=>18,'salary'=>40000,'pic'=>'2.webp'),
        array('name'=>'Nobita Nobi','age'=>20,'salary'=>20000,'pic'=>'3.png'),
        array('name'=>'Miss Jerry','age'=>29,'salary'=>20000,'pic'=>'profile.jpg'),
    );
    // echo $employees[0]['name'];
    ?>

    <div class="container">
    <div class="row">
        <div class="col-md-12">
            <table class="table table-info table-striped table-hover">
                <thead class="thead-dark">
                    <tr>
                        <th>Sr. No</th>
                        <th>Image</th>
                        <th>Name</th>
                        <th>Age</th>
                        <th>Salary</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
                    for($i=0; $i<count($employees);$i++){
                        echo '<tr>';
                        echo '<td>'.$i.'</td>';
                        echo '<td><img src="'.$employees[$i]['pic'].'" style="height:200px;width:150px;"></td>';
                        echo '<td>'.$employees[$i]['name'].'</td>';
                        echo '<td>'.$employees[$i]['age'].'</td>';
                        echo '<td>&#8377;'.$employees[$i]['salary'].'</td>';
                        echo '</tr>';
                    }
                ?>
                </tbody>
            </table>
        </div>
    </div>
    </div>
    

    
</body>
</html>