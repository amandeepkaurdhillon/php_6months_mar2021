<?php 

$employees = array(
    array('name'=>'Peter Parker','age'=>22,'salary'=>30000),
    array('name'=>'Schinchan Nohara','age'=>18,'salary'=>40000),
    array('name'=>'Nobita Nobi','age'=>20,'salary'=>20000),
);
// echo '<pre>';
// print_r( $employees);
// echo '</pre>';

// Access particular value
print_r( $employees[0]['name'].'<br/>');
print_r( $employees[2]['name'].' earns Rs.'.$employees[2]['salary'].'/-<br/>');
?>