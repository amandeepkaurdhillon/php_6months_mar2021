<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My Website</title>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <script src="../jquery.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../fontawesome/css/all.min.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
</head>
<body class="bg-secondary">
    <div class="container-fluid mt-5">
        <div class="row">
            <div class="col-md-4">
                <form action="" class="p-5 bg-light " style="box-shadow:0px 0px 10px black;">
                    <h4 class="text-center">Register Here</h4>
                    <div class="form-group">
                        <label for="">Full Name</label>
                        <input type="text" id="name" placeholder="Enter Name" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">Email</label>
                        <input type="email" id="email" placeholder="Enter Email" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">Password</label>
                        <input type="password" id="password" placeholder="Enter Password" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">Select Gender</label>
                        <select id="gender" class="form-control">
                            <option value="male">Male</option>
                            <option value="female">Female</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success" type="button" id="signUp">Click To Continue</button>
                    </div>
                </form>
            </div>
            <div class="col-md-8">
                <table class="table table-striped table-hover table-light">
                    <thead class="thead-dark">
                        <tr>
                            <th>Sr. No</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Gender</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody id="result"></tbody>
                </table>
                <div class="modal fade" id="editModal">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4>Edit Details</h4>
                            </div>  
                            <div class="modal-body">
                            <input type="hidden" id="editID">
                                <div class="form-group">
                                    <input type="text" id="e-name" placeholder="Enter Name" class="form-control">
                                </div>
                                <div class="form-group">
                                    <input type="email" id="e-email" placeholder="Enter Email" class="form-control">
                                </div>
                                <div class="form-group">
                                    <select id="e-gender" class="form-control">
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-warning btn-sm" type="button" id="signUp" onclick="UpdateRecord()">Save Changes</button>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function getData(){
            $.ajax({
                url:'backend.php',
                type:'get',
                data:{'get_data':''},
                success:function(data){
                    var r = JSON.parse(data);
                    if(r[0].data==0){
                        $('#result').html(r[0].message);
                    }else{
                        var res = '';
                        var count=1;
                        for(i in r){
                            res+='<tr>';
                            res+='<td>'+count+'</td>';
                            res+='<td>'+r[i].u_name+'</td>';
                            res+='<td>'+r[i].u_email+'</td>';
                            res+='<td>'+r[i].u_gender+'</td>';
                            res+='<td>';
                            res+='<button onclick="editData('+r[i].id+')" class="btn btn-primary btn-sm mr-2"><i class="fas fa-edit"></i></button>';
                            res+='<button class="btn btn-danger btn-sm" onclick="deleteRecord('+r[i].id+')"><i class="fas fa-trash"></i></button>';
                            res+='</td>';
                            res+='</tr>';
                            count++;
                        }
                        $('#result').html(res);
                    }
                },
                error:function(err){
                    console.log(err);
                }
            })
        }
        getData();

        setInterval(getData,1000);

        $('#signUp').click(function(){
            var name = $("#name").val();
            var email = $("#email").val();
            var pass = $("#password").val();
            var gen = $("#gender").val();

            $.ajax({
                url:'backend.php',
                type:'post',
                data:{'register':'register','name':name,'email':email,'password':pass,'gender':gen},
                success:function(data){
                    alert(data);
                },
                error:function(data){
                    alert('Something went wrong!')
                }
            })
        });

        function deleteRecord(id){
            $.ajax({
                url:'backend.php',
                type:'post',
                data:{delete_id:id},
                success:function(data){
                    alert(data);
                },
                error:function(data){
                    alert(data);
                },
            })
        }

        function editData(id){
            $.ajax({
                url:'backend.php',
                type:'post',
                data:{edit:id},
                success:function(data){
                    if(data!=0){
                        var final = JSON.parse(data);
                        console.log(final.name);
                        $('#e-name').val(final.name);
                        $('#e-email').val(final.email);
                        $('#e-gender').val(final.gender);
                        $('#editID').val(final.id);
                        $('#editModal').modal('show');
                    }else{
                        alert("No User found on database!");
                    }
                }
            })
        }

        function UpdateRecord(){
            var id = $('#editID').val();
            var name = $('#e-name').val();
            var email = $('#e-email').val();
            var gen = $('#e-gender').val();

            $.ajax({
                url:'backend.php',
                type:'post',
                data:{update:id, name:name, email:email, gender:gen},
                success:function(data){
                    var d = JSON.parse(data);
                    if(d.status==1){
                        swal("Saved!",d.message,"success");
                    }else{
                        swal("Error!", d.message,"error")
                    }
                },
                error:function(err){
                    swal("Saved!",data,"warning");
                }
            })
        }
    </script>
</body>
</html>