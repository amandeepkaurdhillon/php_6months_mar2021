<?php 
    $student = array(
        'name'=>'Peter Parker',
        'email'=>'peter.parker@gmail.com',
        'age'=>25,
        'course'=>'PHP',
        'result'=>'P',
        'profile pic'=>'https://upload.wikimedia.org/wikipedia/en/0/0f/Tom_Holland_as_Spider-Man.jpg',
    );

    // // METHOD 1
    // // It will access array values
    // foreach($student as $x){
    //     echo $x.'<br/>';
    // }

//  // METHOD 2
 //   // It will access both keus and values

    foreach($student as $k=>$v){
        if($k=='profile pic'){
            echo '<img src="'.$v.'" style="height:200px;">';
        }else{
            echo $k.' = '.$v.'<br/>';
        }
    }

?>