<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Array</title>
    <style>
    .box{
        height:100px;width:100%;
    }
    </style>
</head>
<body>
    <table border="1" width="80%" cellspacing="0" cellpadding="20" align="center">
        <thead>
            <tr>
                <th>Index</th>
                <th>Color Name</th>
                <th>Color Demo</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $arr = array("red","green","orange","blue","magenta",
"rgb(12,46,144)","#f5b760","black");
                // echo "Total Number of elements: ".count($arr)."<br/><br/>";
                for($index=0; $index<count($arr); $index++){
                    echo "<tr>";
                    echo "<td>".$index."</td>";
                    echo "<td>".$arr[$index]."</td>";
                    echo "<td><div class='box' style='background:".$arr[$index]."'></div></td>";
                    // echo $index." = ".$arr[$index]."<br/>";
                    echo "</tr>";
                }
            ?>
        </tbody>
    </table>
</body>
</html>