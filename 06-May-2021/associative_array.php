<?php 
    $student = array(
        'name'=>'Peter Parker',
        'email'=>'peter.parker@gmail.com',
        'age'=>25,
        'course'=>'PHP',
        'result'=>'P',
        'profile pic'=>'https://upload.wikimedia.org/wikipedia/en/0/0f/Tom_Holland_as_Spider-Man.jpg',
    );

    echo '<pre>';
    print_r($student);
    echo '</pre>';

    echo $student['name'].' is '.$student['age'].' years old';

?>