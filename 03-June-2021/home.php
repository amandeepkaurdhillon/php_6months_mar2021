<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My Website</title>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <script src="../jquery.js"></script>
    <link rel="stylesheet" href="../fontawesome/css/all.min.css"/>
</head>
<body class="bg-secondary">
    <div class="container-fluid mt-5">
        <div class="row">
            <div class="col-md-4">
                <form action="" class="p-5 bg-light " style="box-shadow:0px 0px 10px black;">
                    <h4 class="text-center">Register Here</h4>
                    <div class="form-group">
                        <label for="">Full Name</label>
                        <input type="text" id="name" placeholder="Enter Name" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">Email</label>
                        <input type="email" id="email" placeholder="Enter Email" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">Password</label>
                        <input type="password" id="password" placeholder="Enter Password" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">Select Gender</label>
                        <select id="gender" class="form-control">
                            <option value="male">Male</option>
                            <option value="female">Female</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success" type="button" id="signUp">Click To Continue</button>
                    </div>
                </form>
            </div>
            <div class="col-md-8">
                <table class="table table-striped table-hover table-light">
                    <thead class="thead-dark">
                        <tr>
                            <th>Sr. No</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Gender</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody id="result"></tbody>
                </table>
            </div>
        </div>
    </div>
    <script>
        function getData(){
            $.ajax({
                url:'backend.php',
                type:'get',
                data:{'get_data':''},
                success:function(data){
                    var r = JSON.parse(data);
                    if(r[0].data==0){
                        $('#result').html(r[0].message);
                    }else{
                        var res = '';
                        var count=1;
                        for(i in r){
                            res+='<tr>';
                            res+='<td>'+count+'</td>';
                            res+='<td>'+r[i].u_name+'</td>';
                            res+='<td>'+r[i].u_email+'</td>';
                            res+='<td>'+r[i].u_gender+'</td>';
                            res+='<td>';
                            res+='<button class="btn btn-primary btn-sm mr-2"><i class="fas fa-edit"></i></button>';
                            res+='<button class="btn btn-danger btn-sm" onclick="deleteRecord('+r[i].id+')"><i class="fas fa-trash"></i></button>';
                            res+='</td>';
                            res+='</tr>';
                            count++;
                        }
                        $('#result').html(res);
                    }
                },
                error:function(err){
                    console.log(err);
                }
            })
        }
        getData();

        setInterval(getData,1000);

        $('#signUp').click(function(){
            var name = $("#name").val();
            var email = $("#email").val();
            var pass = $("#password").val();
            var gen = $("#gender").val();

            $.ajax({
                url:'backend.php',
                type:'post',
                data:{'register':'register','name':name,'email':email,'password':pass,'gender':gen},
                success:function(data){
                    alert(data);
                },
                error:function(data){
                    alert('Something went wrong!')
                }
            })
        });

        function deleteRecord(id){
            $.ajax({
                url:'backend.php',
                type:'post',
                data:{delete_id:id},
                success:function(data){
                    alert(data);
                },
                error:function(data){
                    alert(data);
                },
            })
        }
    </script>
</body>
</html>