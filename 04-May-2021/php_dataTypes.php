<?php 

    $a = 10;
    $b = 10.5;
    $c = 'Hello';
    $d = true;
    $e = ['red','green','blue'];
    $f = null;

    echo var_dump($a).'<br/>';
    echo var_dump($b).'<br/>';
    echo var_dump($c).'<br/>';
    echo var_dump($d).'<br/>';
    echo var_dump($e).'<br/>';
    echo var_dump($f).'<br/>';

?>