<?php 

    $txt = 'php stands for Hypertext Preprocessor';
    echo $txt.'<br/><br/>';

    echo strtoupper($txt.'<br/><br/>');
    echo strtolower($txt.'<br/><br/>');
    echo ucfirst($txt.'<br/><br/>');
    echo ucwords($txt.'<br/><br/>');
    // echo substr($txt,5).'<br/>';
    echo substr($txt,4,5).'<br/><br/>';
    echo strlen($txt).'<br/><br/>';
    echo strlen('hello').'<br/><br/>';

    $x = 'I love HTML';
    echo $x[0].'<br/><br/>';  
    echo $x.'<br/><br/>';  
    echo str_replace('HTML','PHP',$x);
    
?>