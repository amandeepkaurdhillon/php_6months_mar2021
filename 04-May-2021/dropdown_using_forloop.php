<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Dropdown</title>
</head>
<body>
    <label for="">Select Date</label>
    <select name="" id="">
        <?php 
            for($date=1; $date<=31; $date++){
                echo '<option>'.$date.'</option>';
            }
        ?>
    </select>
    <label for="">Select Year</label>
    <select name="" id="">
        <?php 
            for($y=2021; $y>=1990; $y--){
                echo '<option>'.$y.'</option>';
            }
        ?>
    </select>
</body>
</html>