<?php

class Student{
    // Data members
    public $college = "Abcd Engg College";

    //Member function
    function print_name($name){
        echo $name. " is studying in ".$this->college."<br>";

    }
}

// Create Object 

$st1 = new Student();
$st1->print_name("Aman");

$st2 = new Student();
$st1->print_name("James ");

echo $st2->college;

?>