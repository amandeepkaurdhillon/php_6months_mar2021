<?php 
    class Dog{
        // private $no_of_eyes = 2;
        // protected $no_of_eyes = 2;
        public $no_of_eyes = 2;
        public function intro(){
            echo "Dogs are really loyal pet";
        }
    }
    class Bulldog extends Dog{
        public $breed = "Bulldog";
        public function details(){
            echo "<h1>These kind of dogs are little aggressive!</h1>";
        }
    }

    $ob =  new Bulldog();
    echo $ob->breed;
    $ob->details();
    echo "Eyes = ".$ob->no_of_eyes."<br/>";
    $ob->intro();
    // $obj = new Dog();
    // echo $obj->no_of_eyes;
    // $obj->intro();
    // $obj->details(); // Parent object can't access child's attributes

?>