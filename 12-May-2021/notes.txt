Access Specifiers 

public - All over (acess all accross)
private - only within the class (even not child class )
protected - class + child class 

Constructor 
    - A member function which calls automatically on object creation
    - It is used to initialize values 
    - in PHP Constructor function starts with double underscore (__)


Inheritance - when a class created (derived) from another class 
    parent class - base class 
    child class - derived class 
    