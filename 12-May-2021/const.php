<?php 

    class Circle{

        // PHP Constructor
        function __construct($r){
            $this->radius = $r;
        }
        function cal_area(){
            return pi()*$this->radius*$this->radius;
        }

    }

    $c1 = new Circle(10);
    $c2 = new Circle(0);

    echo $c1->cal_area()."<br/>";
    echo $c2->cal_area()."<br/>";

?>