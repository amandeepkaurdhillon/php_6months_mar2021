<?php
 class Employee{
     public $company_name = "XYZ Corporate Ltd.";

     public function set_profile($name,$sal){
        $this->name = $name;
        $this->salary = $sal;
     }

     function get_profile(){
        return $this->name." earns ".$this->salary." per annum";       
     }
 }

//  Create object 

$emp1 = new Employee();
$emp2 = new Employee();
$emp3 = new Employee();

// Set name
$emp1->set_profile("Peter Parker","$100000");
$emp2->set_profile("James Parker","$5000000");
$emp3->set_profile("Jenny","$300000");

//get profile 
echo $emp1->get_profile()."<br/>";
echo $emp2->get_profile()."<br/>";
echo $emp3->get_profile()."<br/>";
?>