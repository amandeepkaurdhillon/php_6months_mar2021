<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register Form</title>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
</head>
<body class="bg-secondary">
<div class="container bg-light mt-5 w-75 p-5">
    <div class="alert alert-primary m-3 p-3">
    <?php
        // if(isset($_GET["fname"])){
        //     echo "Welcome ".$_GET["fname"]."!<br/>";
        //     echo $_GET["email"].'<br/>';
        //     echo $_GET["password"].'<br/>';
        //     echo $_GET["contact"].'<br/>';
        // }
    ?>
    </div>
    <div class="row">
        <div class="col-md-12">
        <h1 class="text-uppercase text-center mb-3">Register Here</h1>
            <form action="data.php" method="post">
                <div class="form-group">
                    <label>Full Name</label>
                    <input type="text" class="form-control" name="fname">
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input type="email" class="form-control" name="email">
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input type="password" class="form-control" name="password">
                </div>
                <div class="form-group">
                    <label>Contact Number</label>
                    <input type="text" class="form-control" pattern="[0-9]{10}" title="Contact Number must contain 10 digits!" name="contact">
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-success">
                </div>
            </form>        
        </div>
    </div>    
</div>
</body>
</html>