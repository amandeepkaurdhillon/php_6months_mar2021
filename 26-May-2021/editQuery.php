<?php require('../21-May-2021/header.php');?>

<?php 
$con = new mysqli('localhost','root', '', 'batch2021');

if(isset($_GET['id'])){
    $id = $_GET['id'];
    $qry = "SELECT * FROM register WHERE id='$id'";

    $query = $con->query($qry);
    if($query->num_rows>0){
        $data = $query->fetch_assoc();
        print_r($data);
    }else{
        die('<h1 style="color:red;">Could not fetch details of this ID!</h1>');
    }
}else{
    $data=[];
    $data['name']='';
    $data['email']='';
}
    
if(isset($_POST['editBtn'])){
    $id= $_GET['id'];
    $new_name = $_POST['full_name'];
    $new_email = $_POST['email'];
    $new_gender = $_POST['gender'];

    $qry = "UPDATE register SET name='$new_name', email='$new_email', gender='$new_gender' WHERE id='$id'";

    if($con->query($qry)){
        header('Location:../21-May-2021/all_users.php');
    }else{
        echo '<h1 class="alert alert-danger">Could not update!</h1>';
    }
} 

?>
<div>
<div class="container p-4 my-5 w-50 text-light" style="background:rgba(0,0,0,0.5);">
<div class="row">
            <div class="col-md-12">
                <h3 class="text-light text-center">Edit Profile</h3>
            </div>
            <div class="col-md-12">
                <form action="" method="post">
                    <div class="form-group">
                        <label for="">Enter Name</label>
                        <input type="text" name="full_name" class="form-control" value="<?php echo $data['name'];?>">
                    </div>
                    <div class="form-group">
                        <label for="">Enter Email</label>
                        <input type="email" name="email" class="form-control" value="<?php echo $data['email'];?>">
                    </div>
                 
                    <div class="form-group">
                    <label for="">Select Gender</label>
                    <select id="" name="gender" class='form-control'>
                    <?php 
                    $gender = $data['gender'];
                    if($gender=='male'){
                        echo '<option value="male" selected >Male</option>';
                    }else{
                        echo '<option value="male" >Male</option>';
                    }
                    if($gender=='female'){
                        echo '<option value="female" selected >Female</option>';
                    }else{
                        echo '<option value="female" >Female</option>';
                    }
                    ?>
                        
                    </select>    
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Save Changes" class="btn btn-success" name="editBtn">
                    </div>
                </form>
            </div>
        </div>
</div>
<?php require('../21-May-2021/footer.php'); ?>


