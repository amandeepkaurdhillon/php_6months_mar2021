<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Validation</title>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
</head>
<body style="background:url('../images/nature.jpg');background-size:cover;" class="pt-5">
    <div class="container w-50 p-4 mt-5" style="background:red;color:white;">
        <div class="row">
            <div class="col-md-12">
                <?php
                if(isset($_POST['full_name'])){
                    if($_POST['full_name']==""){
                        echo "<p>*Name is required!</p>";
                    }
                    else if(!preg_match("/^[A-Za-z-]*$/", $_POST['full_name'])){
                        echo '<p class="alert alert-warning">Name must contain letters only!</p>';
                    }
                    else{
                        echo '<p>WELCOME '.$_POST['full_name'].'!!!</p>';
                    }

                    if ($_POST['email']==''){
                        echo '<p>*Email is reuired</p>';
                    }
                    if ($_POST['password']==''){
                        echo '</p>*Password is reuired</p>';
                    }
                    if ($_POST['cpassword']==''){
                        echo '</p>*Confirm Password is reuired</p>';
                    }

                    if($_POST['password']!=$_POST['cpassword']){
                        echo "<p class='alert alert-danger'>Password doesn't match!</p>";
                    }
                }
                ?>
                <form action="" method="post">
                    <div class="form-group">
                        <label for="">Enter Name</label>
                        <input type="text" name="full_name" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">Enter Email</label>
                        <input type="email" name="email" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">Enter Password</label>
                        <input type="password" name="password" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">Confirm Password</label>
                        <input type="password" name="cpassword" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Sign Up" class="btn btn-success">
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>