<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My Website</title>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <script src="../jquery.js"></script>
</head>
<body>
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-8">
                <table class="table table-striped table-hover">
                    <thead class="thead-dark">
                        <tr>
                            <th>Sr. No</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Gender</th>
                        </tr>
                    </thead>
                    <tbody id="result"></tbody>
                </table>
            </div>
        </div>
    </div>
    <script>
        function getData(){
            $.ajax({
                url:'backend.php',
                type:'get',
                data:{'get_data':''},
                success:function(data){
                    var r = JSON.parse(data);
                    if(r[0].data==0){
                        $('#result').html(r[0].message);
                    }else{
                        var res = '';
                        var count=1;
                        for(i in r){
                            res+='<tr>';
                            res+='<td>'+count+'</td>';
                            res+='<td>'+r[i].u_name+'</td>';
                            res+='<td>'+r[i].u_email+'</td>';
                            res+='<td>'+r[i].u_gender+'</td>';
                            res+='</tr>';
                            count++;
                        }
                        $('#result').html(res);
                    }
                },
                error:function(err){
                    console.log(err);
                }
            })
        }
        getData();

        setInterval(getData,1000);
    </script>
</body>
</html>