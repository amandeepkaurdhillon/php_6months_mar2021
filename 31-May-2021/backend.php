<?php 
$con = new mysqli('localhost','root','','batch2021');

if(isset($_GET['get_data'])){
    $arr = array();

    $qry = "SELECT * FROM register";
    $res = $con->query($qry);
    if($res->num_rows>0){
        while($row=$res->fetch_assoc()){
            $aa = array(
                'id'=>$row['id'],
                'u_name'=>$row['name'],
                'u_email'=>$row['email'],
                'u_gender'=>$row['gender'],
            );
            array_push($arr,$aa);
        }
    }else{
        $response = array(
            'status'=>'success',
            'message'=>'No record found!',
            'data'=>0,
        );
        array_push($arr,$response);
    }

    echo json_encode($arr);
}
?>