<?php include('header.php'); ?>
<?PHP
// session_start();
if(isset($_SESSION['login'])){
    if($_SESSION['login']==false){
        echo 'You are not allowed!';
        exit();
    }
}else{
    echo '<h1 class="jumbotron p-5 m-5">Please Login First!</h1>';
        exit();
}
?>


<section>
<div class="container p-4 mt-5" style="background:rgba(0,0,0,0.5);">
    <div class="row">
        <div class="col-md-12">
            <?php 
                include('../20-May-2021/db_connect.php');
                $qry = "SELECT * FROM register";
                $result = $con->query($qry);
                if($result->num_rows>0){
                    echo '<h1 class="text-light text-center">All Users ('.$result->num_rows.')</h1>';
                    echo '<table class="table table-info table-striped mt-4" id="all_users">';
                    echo '<thead>';
                        echo '<tr>';
                        echo '<th>Sr No.</th>';
                        echo '<th>Name</th>';
                        echo '<th>Email</th>';
                        echo '<th>Gender</th>';
                        echo '<th>Action</th>';
                        echo '</tr>';
                    echo '</thead>';
                    echo '<tbody>';
                    $count=1;
                    while($row= $result->fetch_assoc()){
                        echo '<tr>';
                        echo '<td>'.$count.'</td>';
                        echo '<td>'.$row['name'].'</td>';
                        echo '<td>'.$row['email'].'</td>';
                        echo '<td>'.$row['gender'].'</td>';
                        echo '<td>';
                        echo '<a href="../26-May-2021/editQuery.php?id='.$row['id'].'" class="btn btn-primary btn-sm text-light mr-3"><i class="fas fa-edit"></i> Edit<a>';
                        echo '<a href="../27-May-2021/deleteQuery.php?id='.$row['id'].'" class="btn btn-danger btn-sm text-light"><i class="fas fa-trash"></i> Delete<a>';
                        echo '</td>';
                        echo '</tr>';
                        $count++;
                    }
                    echo '</tbody>';
                    echo '</table>';
                    echo '<script>$(document).ready( function () {$("#all_users").DataTable();} );</script>';
                }else{
                    echo '<h1 class="alert alert-info">No Record Found!</h1>';
                }
            ?>
        </div>
    </div>
</div>
</section>
<script>
    $(document).ready( function () {
    $('#all_users').DataTable();
} );
</script>
<?php include('footer.php'); ?>