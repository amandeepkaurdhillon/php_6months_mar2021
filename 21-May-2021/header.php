<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP CRUD</title>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <script src="../jquery.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="../fontawesome/css/all.min.css">
    <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <style>
        .links li a{
            text-decoration:none;
            color:white;
            text-transform: uppercase;
        }
        .conact_us li{
            line-height: 40px;
        }
        body{
            background:url('../images/nature.jpg');background-size:cover;
        }
    </style>
</head>
<body class="h-100">
    
    <header>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 px-0">
                    <nav class="navbar bg-dark navbar-dark navbar-expand">
                        <a href="" class="navbar-brand"> My Site</a>
                        <ul class="navbar-nav mx-auto text-uppercase">
                            <li class="nav-item">
                                <a href="" class="nav-link">About Us</a>
                            </li>
                            <?php 
                                session_start();
                                if((!isset($_SESSION['login']))||($_SESSION['login']==false)){
                            ?>
                            <li class="nav-item">
                                <a href="../20-May-2021/insert_query.php" class="nav-link">Register</a>
                            </li>
                            <li class="nav-item">
                                <a href="../28-May-2021/login.php" class="nav-link">Login</a>
                            </li>
                            <?php 
                                }else{
                            ?>
                           <li class="nav-item">
                                <a href="../28-May-2021/dashboard.php" class="nav-link">Dashboard</a>
                            </li>
                           <?php 

                                }
                            ?>

                            <li class="nav-item">
                                <a href="../21-May-2021/all_users.php" class="nav-link">All Users</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </header>