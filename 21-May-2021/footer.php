<footer>
    <div class="container-fluid" >
        <div class="row bg-dark text-light">
            <div class="col-md-4">
                <h2>My Site</h2>
                <p><small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum repellat fugiat, debitis et exercitationem impedit. Numquam dolores vero pariatur eum quaerat odit deserunt quia velit. Autem culpa ullam consectetur impedit?</small></p>
            </div>
            <div class="col-md-4 links">
                <h4>LINKS</h4>
                <ul>
                    <li><a href="">About Us</a></li>
                    <li><a href="">Login</a></li>
                    <li><a href="../20-May-2021/insert_query.php">Register</a></li>
                    <li><a href="">All Users</a></li>
                </ul>
            </div>
            <div class="col-md-4 conact_us">
                <h4>CONTACT US</h4>
                <ul type="none">
                    <li><em>info@mysite.com</em></li>
                    <li><em>+91 9999999999</em></li>
                    <li><em>https://facebook.com/abcd</em></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
</body>
</html>