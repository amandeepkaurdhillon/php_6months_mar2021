<?php include('../21-May-2021/header.php'); ?>
<?PHP
// session_start();
if(isset($_SESSION['login'])){
    if($_SESSION['login']==false){
        echo 'You are not allowed!';
        exit();
    }
}else{
    echo '<h1 class="jumbotron p-5 m-5">Please Login First!</h1>';
    exit();
}
?>
<div class="container my-5">
    <div class="row">
        <div class="col-md-12 card">
            <div class="card-header">
                <h4>Welcome To Dashboard!</h4>
            </div>
            <div class="card-body">
                <p><?php echo $_SESSION['user_email'];?>You are logged in! <a href="logout.php">Click to Logout</a></p>

            </div>

        </div>
    </div>
</div>

<?php include('../21-May-2021/footer.php'); ?>