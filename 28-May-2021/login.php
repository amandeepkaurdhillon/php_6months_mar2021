<?php include('../21-May-2021/header.php') ?>
<section>
<div class="container p-4 my-5 w-50 text-light" style="background:rgba(0,0,0,0.5);">
    <div class="row">
    <div class="col-md-12">
    <h1 class="text-light text-center">Login Here</h1>
    <?php 
    // session_start();
        $con = new mysqli('localhost','root','','batch2021');
        if(isset($_POST['login'])){
            $email = $_POST['email'];
            $password = md5($_POST['password']);

            $qry = "SELECT * FROM register WHERE email='$email' and password='$password'";
            $result = $con->query($qry);
            
            if($result->num_rows==0){
                echo '<p class="alert alert-danger">Incorrect login details!</p>';
                $_SESSION['login']=false;
            }else{
                $_SESSION['login']=true;
                $_SESSION['user_email']=$email;
                header('Location:../28-May-2021/dashboard.php');
            }
        }

    ?>
        <form action="" method="post">
            <div class="form-group">
                <label for="">Enter Email</label>
                <input type="email" class="form-control" name="email">
            </div>
            <div class="form-group">
                <label for="">Enter Password</label>
                <input type="password" class="form-control" name="password">
            </div>
            <div class="form-group">
                <input type="checkbox">
                <label for="">Remember Me</label>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-success" value="Log In" name="login">
            </div>
            <div class="form-group text-light">
                <p>Don't Have an account?
                <a href="../20-May-2021/insert_query.php">Click to Create Account</a>
                </p>
            </div>
        </form>
    </div>
    </div>
</div>
</section>
<?php include('../21-May-2021/footer.php') ?>