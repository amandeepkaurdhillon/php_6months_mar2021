<?php

$url = 'https://restcountries.eu/rest/v2/all';
$data = file_get_contents($url);

$phpArr = json_decode($data, true);

// echo '<pre>';
// print_r($phpArr);
// echo '</pre>';
echo '<ol>';
foreach($phpArr as $k){
    echo '<li>'.$k['name'].' - '.$k['capital'].'</li>';
}
echo '</ol>';
?>