<?php 
    $url = file_get_contents('https://restcountries.eu/rest/v2/all');
    $toPhp = json_decode($url, true);    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>REST COUNTRIES</title>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mt-3">
                <table class="table table-hover table-primary table-striped">
                    <thead class="thead-dark">
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Capital</th>
                            <th>Population</th>
                            <th>Borders</th>
                            <th>Languages</th>
                            <th>Flag</th>
                        </tr>
                    </thead>
                
                    <tbody>
                        <?php
                        for($i=0; $i<count($toPhp); $i++){
                            $b = $toPhp[$i]['borders'];

                            echo '<tr>';
                            echo '<td>'.($i+1).'</td>';
                            echo '<td>'.$toPhp[$i]['name'].'</td>';
                            echo '<td>'.$toPhp[$i]['capital'].'</td>';
                            echo '<td>'.$toPhp[$i]['population'].'</td>';
                            echo '<td><ol>';
                            for($j=0; $j<count($b); $j++){
                                echo '<li>'.$b[$j].'</li>';
                            }
                            echo '</ol></td>';
                            
                            echo '<td><ul>';
                            for($l=0; $l<count($toPhp[$i]['languages']); $l++){
                                echo '<li>'.$toPhp[$i]['languages'][$l]['name'].'</li>';
                            }
                            echo '</ul></td>';
                            echo '<td><img src="'.$toPhp[$i]['flag'].'" style="height:200px;width:300px;"/></td>';
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>